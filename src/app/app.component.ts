import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'thelp-countdown';

  ngOnInit(){
		// set the date we're counting down to
		var target_date = new Date('April, 15, 2019 11:30:00').getTime();
		// variables for time units
		var days, hours, minutes, seconds;
		// get tag element
		var countdown = document.getElementById('countdown');
		// update the tag with id "countdown" every 1 second
		setInterval(function () {
	    // find the amount of "seconds" between now and target
	    var current_date = new Date().getTime();
	    var seconds_left = (target_date - current_date) / 1000;
	    // do some time calculations
	    days = seconds_left / 86400;
	    seconds_left = seconds_left % 86400;
	    hours = seconds_left / 3600;
	    seconds_left = seconds_left % 3600;
	    minutes = seconds_left / 60;
	    seconds = seconds_left % 60;
			countdown.innerHTML = 
				'<div class="row">'+
						'<div class="col-md-7 offset-4">'+
							'<div class="row">'+
								'<div class="col-md-12 mat-elevation-z4 card days m-1 mb-3" style="text-align: center; padding: 1em;">'+
									'<h2> <span style="color: #da5c04">WE WILL BE </span> <span style="color: #2a1e41"> BACK SOON </span></h2>'+
								'</div>'+
							'</div>'+
							'<div class="row">'+
								'<div class="col-md-3">'+
									'<div class="mat-elevation-z4 card days m-1" style="text-align: center; padding:1em 0;">'+ 
										'<h4 style="color: #da5c04">' + Math.floor(days) +  '</h4>'+
										'<label style="color: #2a1e41">Days</label>'+
									'</div>'+
								'</div>'+
								'<div class="col-md-3">'+
									'<div class="mat-elevation-z4 card hours m-1" style="text-align: center; padding:1em 0;">'+ 
										'<h4 style="color: #da5c04">' + Math.floor(hours) + '</h4>'+
										'<label style="color: #2a1e41">Hours</label>'+
									'</div>'+
								'</div>'+
								'<div class="col-md-3">'+
									'<div class="mat-elevation-z4 card minutes m-1" style="text-align: center; padding:1em 0;">'+ 
										'<h4 style="color: #da5c04">' + Math.floor(minutes) + '</h4>'+
										'<label style="color: #2a1e41"> Minutes</label>'+
									'</div>'+
								'</div>'+
								'<div class="col-md-3">'+
									'<div class="mat-elevation-z4 card seconds m-1" style="text-align: center; padding:1em 0;">'+ 
										'<h4 style="color: #da5c04">' + Math.floor(seconds) + '</h4>'+
										'<label style="color: #2a1e41">Seconds</label>'+
									'</div>';
								'</div>'+
							'</div>'+
						'</div>'+
				'</div>'
		}, 1000);
  }
}
